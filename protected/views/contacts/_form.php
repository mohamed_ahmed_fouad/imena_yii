<?php
/* @var $this ContactsController */
/* @var $model Contacts */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contacts-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'number'); ?>
		<?php echo $form->textField($model,'number',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->radioButtonList($model, 'type', array('0'=>'Personal', '1'=>'Business'), array('onchange'=>'js:checkRadioType()')); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row personal_frm">
		<?php echo $form->labelEx($model,'photo_ext'); ?>
		<?php echo $form->fileField($model,'photo_ext',array('size'=>5,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'photo_ext'); ?>
	</div>

	<div class="row personal_frm">
		<?php echo $form->labelEx($model,'home_address'); ?>
		<?php echo $form->textField($model,'home_address',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'home_address'); ?>
	</div>

	<div class="row personal_frm">
		<label><?php echo $model->getAttributeLabel('birth_day'); ?> (<?php echo $model->rules()[6]["format"]; ?>)</label>
		<?php echo $form->textField($model,'birth_day'); ?>
		<?php echo $form->error($model,'birth_day'); ?>
	</div>

	<div class="row business_frm">
		<?php echo $form->labelEx($model,'fax'); ?>
		<?php echo $form->textField($model,'fax',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'fax'); ?>
	</div>

	<div class="row business_frm">
		<?php echo $form->labelEx($model,'website'); ?>
		<?php echo $form->textField($model,'website'); ?>
		<?php echo $form->error($model,'website'); ?>
	</div>

	<?php if(CCaptcha::checkRequirements()): ?>
		<div class="row">
			<?php echo $form->labelEx($model,'verifyCode'); ?>
			<div>
				<?php $this->widget('CCaptcha', array('captchaAction' => 'contacts/captcha')); ?>
				<?php echo $form->textField($model,'verifyCode'); ?>
			</div>
			<div class="hint">Please enter the letters as they are shown in the image above.
				<br/>Letters are not case-sensitive.</div>
			<?php echo $form->error($model,'verifyCode'); ?>
		</div>
	<?php endif; ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
	function checkRadioType() { // Checks Radio buttons for changes to toggle the input form Personal to Business
		if(document.getElementById('Contacts_type_0').checked) {
			ChangeInputs('personal_frm', 'block');
			ChangeInputs('business_frm', 'none');
		} else if (document.getElementById('Contacts_type_1').checked) {
			ChangeInputs('personal_frm', 'none');
			ChangeInputs('business_frm', 'block');
		}
	}

	function ChangeInputs(selector, display_val) {
		var input_class = document.getElementsByClassName(selector);
		for (var i = 0; i < input_class.length; ++i) {
			var item = input_class[i];
			item.style.display = display_val;
		}
	}

	checkRadioType();

</script>