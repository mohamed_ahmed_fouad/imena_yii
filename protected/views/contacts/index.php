<?php
/* @var $this ContactsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Contacts',
);

$this->menu=array(
	array('label'=>'Create Contacts', 'url'=>array('create')),
	array('label'=>'Manage Contacts', 'url'=>array('admin')),
);
?>

<h1>Contacts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'random')); ?>
	<?php if($random_model) { ?>
	<h3>Random Contact</h3>

	<?php if($random_model->type == 1) {
			$image = "photos/default.jpg";
		} else {
			$image = "photos/" . $random_model->id . "." . $random_model->photo_ext;
		} ?>
	<img src="<?php echo $image; ?>" style="max-width: 50px; max-height: 50px;">
	<p><?php echo $random_model->name; ?></p>
	<?php } ?>
<?php $this->endWidget();?>