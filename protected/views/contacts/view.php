<?php
/* @var $this ContactsController */
/* @var $model Contacts */

$this->breadcrumbs=[
	'Contacts'=>['index'],
	$model->name,
];

$this->menu=[
	['label'=>'List Contacts', 'url'=>['index']],
	['label'=>'Create Contacts', 'url'=>['create']],
	['label'=>'Update Contacts', 'url'=>['update', 'id'=>$model->id]],
	['label'=>'Delete Contacts', 'url'=>'#', 'linkOptions'=>['submit'=>['delete','id'=>$model->id],'confirm'=>'Are you sure you want to delete this item?']],
	['label'=>'Manage Contacts', 'url'=>['admin']],
];
?>

<h1>View Contacts #<?php echo $model->id; ?></h1>

<?php
	$attributes = ['id', 'name', 'email', 'number'];
	if($model->type == 0) {
		$attributes = array_merge($attributes, ['home_address', 'birth_day']);
	} else {
		$attributes = array_merge($attributes, ['fax', 'website']);
	}
	$this->widget('zii.widgets.CDetailView', [
	'data'=>$model,
	'attributes'=> $attributes,
]); ?>

<?php
	if($model->type == 0) {
		$this->beginWidget('zii.widgets.jui.CJuiDialog', [
		'id'=>'mydialog',
		// additional javascript options for the dialog plugin
		'options' => [
			'title'    => 'Photo & Home Address',
			'autoOpen' => false,
			'modal'    => true,
		],
		]);

		echo '<b>Home Address:</b> ' . $model->home_address;
		echo '<br /> <img src="photos/' . $model->id . '.' . $model->photo_ext . '" style="max-width: 275px;">';

		$this->endWidget('zii.widgets.jui.CJuiDialog');

		// the link that may open the dialog
		echo CHtml::link('the Button', '#', [
			'onclick' => '$("#mydialog").dialog("open"); return false;',
		]);
	} else {
		echo CHtml::link('the Button', 'mailto:'.$model->email.'?Subject=Hello%20iMENA');
	}
?>