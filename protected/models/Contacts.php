<?php

/**
 * This is the model class for table "contacts".
 *
 * The followings are the available columns in table 'contacts':
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $number
 * @property integer $type
 * @property string $photo_ext
 * @property string $home_address
 * @property string $birth_day
 * @property string $fax
 * @property string $website
 */
class Contacts extends CActiveRecord
{
	public $verifyCode;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'contacts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, email, number, type', 'required'),
			array('type', 'numerical', 'integerOnly'=>true),
			array('name, email, home_address, website, photo_ext', 'length', 'max'=>255),
			array('number', 'length', 'max'=>10),
			array('fax', 'length', 'max'=>15),
			array('birth_day', 'safe'),
			array('birth_day', 'date', 'format'=>'yyyy-M-d'),
			array('email', 'email'),
			array('website', 'url'),
			array('type', 'boolean'),
			array('photo_ext', 'file', 'types'=>'jpg,png,jpeg,gif', "allowEmpty" => true),
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, email, number, type, photo_ext, home_address, birth_day, fax, website', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'email' => 'E-mail',
			'number' => 'Phone number',
			'type' => 'Contact Type',
			'photo_ext' => 'Photo',
			'home_address' => 'Home address',
			'birth_day' => 'Birthday',
			'fax' => 'FAX',
			'website' => 'Website',
			'verifyCode'=>'Verification Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('number',$this->number,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('photo_ext',$this->photo_ext,true);
		$criteria->compare('home_address',$this->home_address,true);
		$criteria->compare('birth_day',$this->birth_day,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('website',$this->website);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Contacts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
