SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `imena_db`
--
CREATE DATABASE IF NOT EXISTS `imena_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `imena_db`;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--
-- Creation: Apr 16, 2015 at 07:38 PM
--

CREATE TABLE `contacts` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'Name',
  `email` varchar(255) NOT NULL COMMENT 'E-mail',
  `number` varchar(10) NOT NULL COMMENT 'Phone number',
  `type` tinyint(1) NOT NULL COMMENT 'Contact Type',
  `photo_ext` varchar(255) DEFAULT NULL COMMENT 'Photo',
  `home_address` varchar(255) DEFAULT NULL COMMENT 'Home address',
  `birth_day` datetime DEFAULT NULL COMMENT 'Birthday',
  `fax` varchar(15) DEFAULT NULL COMMENT 'FAX',
  `website` varchar(255) DEFAULT NULL COMMENT 'Website'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;